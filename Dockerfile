FROM openjdk:8u151-jdk-alpine
MAINTAINER Eric <ericsupport@1rs.io>


######################################## node:8.9.4 INSTALLATION STARTS ######################################################
ENV NODE_VERSION 14.18.0

RUN addgroup -g 1000 node \
  && adduser -u 1000 -G node -s /bin/sh -D node \
  && apk add --no-cache \
    binutils-gold curl g++ gcc git gnupg libgcc libstdc++ linux-headers make python python-dev \
  # gpg  keys listed at https://github.com/nodejs/node#release-team
  && for key in \
    8FCCA13FEF1D0C2E91008E09770F7A9A5AE15600 \
  ; do \
    gpg2 --keyserver pgp.mit.edu --recv-keys "$key" || \
    gpg2 --keyserver keyserver.pgp.com --recv-keys "$key" || \
    gpg2 --keyserver ha.pool.sks-keyservers.net --recv-keys "$key" ; \
  done \
    && curl -SLO "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION.tar.xz" \
    && curl -SLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc" \
    && gpg2 --batch --decrypt --output SHASUMS256.txt SHASUMS256.txt.asc \
    && grep " node-v$NODE_VERSION.tar.xz\$" SHASUMS256.txt | sha256sum -c - \
    && tar -xf "node-v$NODE_VERSION.tar.xz" \
    && cd "node-v$NODE_VERSION" \
    && ./configure \
    && make -j$(getconf _NPROCESSORS_ONLN) \
    && make install \
    && cd .. \
    && rm -Rf "node-v$NODE_VERSION" \
    && rm "node-v$NODE_VERSION.tar.xz" SHASUMS256.txt.asc SHASUMS256.txt
######################################## node:8.9.4 INSTALLATION ENDS ######################################################

######################################## GRADLE INSTALLATION STARTS ######################################################
RUN mkdir /usr/lib/gradle /app

ENV GRADLE_VERSION 4.10.3
ENV GRADLE_HOME /usr/lib/gradle/gradle-${GRADLE_VERSION}
ENV PATH ${PATH}:${GRADLE_HOME}/bin

WORKDIR /usr/lib/gradle
RUN set -x \
  && apk add --no-cache wget \
  && wget https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip \
  && unzip gradle-${GRADLE_VERSION}-bin.zip \
  && rm gradle-${GRADLE_VERSION}-bin.zip \
  && apk del wget

######################################## GRADLE INSTALLATION ENDS ######################################################
    
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1
 RUN apk add --update --no-cache build-base ca-certificates curl git py3-cryptography py3-pip python3 python3-dev libffi-dev libressl-dev bash gettext groff less libstdc++ jq zip && \
     curl -O https://bootstrap.pypa.io/get-pip.py && \
     python3 get-pip.py && \
     python3 -m pip install --upgrade six==1.16.0 awscli awsebcli && \
     rm -rf /var/cache/apk/* 
   
 RUN apk --no-cache add python py-pip && \
     python3 -m pip --no-cache-dir install --upgrade pip && \
     python3 -m pip --no-cache-dir install --upgrade --user boto3 boto botocore
  
 RUN apk update && \
     apk add --no-cache git openssh perl && \
     python3 -m pip install pytz tzlocal
 RUN ln -sf /usr/bin/python3.6 /usr/bin/python
 
ENV PATH "$PATH:~/.local/bin"

ADD deployment-scripts /opt/deployment-scripts

